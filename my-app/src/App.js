import Header from './component/layout/Header';
import Footer from './component/layout/Footer';
import { useLocation } from 'react-router-dom';
import MenuLeft from './component/layout/MenuLeft';
import './App.css';
import MenuAcc from './component/layout/MenuAcc';
import { UserContext } from './UserContext';
import { useState } from 'react';

function App(props) {
  const params1 =useLocation()
  const [qty,setQty]= useState()
  
  function getqty(data){
    setQty(data)
  }
  return (
    
    <UserContext.Provider value={{
      qty: qty,
      getqty: getqty
    }}>
      
      <Header />
      <section id="form">
          <div className='container'>
              <div className='row'>
                {params1["pathname"].includes('account')? <MenuAcc/>: params1["pathname"].includes('cart')? "": <MenuLeft/>  }
                {props.children }
              </div>
          </div>
      </section>
     <Footer></Footer>
    </UserContext.Provider>
   
  );
}

export default App;

