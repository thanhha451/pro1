import axios from "axios"
import {useContext, useEffect, useState } from "react"
import { UserContext } from "./UserContext"


function Cart(props){
  const [product, setProduct] = useState([])
  const value = useContext(UserContext)
  
   useEffect(()=>{
    axios.post("http://localhost/laravel8/laravel8/public/api/product/cart", JSON.parse(localStorage.getItem("objectProduct")))
    .then(response=>{
      setProduct(response.data.data)
    })
    .catch(function (error) {
      console.log(error)
   })
   },[])
//  tang qty
  function tangqty(e){
    const id =e.target.id
    const objectProduct = JSON.parse(localStorage.getItem("objectProduct"))
   
    if(objectProduct[id] > 0){
      objectProduct[id] +=1
      
    }
      localStorage.setItem('objectProduct', JSON.stringify(objectProduct))
      axios.post("http://localhost/laravel8/laravel8/public/api/product/cart", objectProduct)
      .then(response=>{
        setProduct(response.data.data)
      })

  }
// giam qty//
  function giamqty(e){
    const id =e.target.id
    const objectProduct = JSON.parse(localStorage.getItem("objectProduct"))
   
    if(objectProduct[id] > 0){
      objectProduct[id] -=1
    
    }
    // xóa phần từ
    if(objectProduct[id] == 0){
    const removeIndex = product.findIndex( item => item.id === id );
    product.splice( removeIndex, 1 );
    delete  objectProduct[id]
   
    }
      localStorage.setItem('objectProduct', JSON.stringify(objectProduct))
      axios.post("http://localhost/laravel8/laravel8/public/api/product/cart", objectProduct)
      .then(response=>{
        setProduct(response.data.data)
      })

  }
  function renderProduct(){
    if(product.length){
      return Object.keys(product).map((key,i)=>{
        return (
          <>
          <tr>
							<td className="cart_product">
								<a href=""><img src={"http://localhost/laravel8/laravel8/public/upload/product/"+product[key]["id_user"]+"/"+JSON.parse(product[key]["image"])["0"]} alt=""/></a>
							</td>
							<td className="cart_description">
								<h4><a href="">{product[key]["name"]}</a></h4>
								<p>Web ID: {product[key]['web_id']}</p>
							</td>
							<td className="cart_price">
								<p>${product[key]["price"]}</p>
							</td>
							<td className="cart_quantity">
								<div className="cart_quantity_button">
									<a className="cart_quantity_up" onClick={tangqty} id={product[key]["id"]} href> + </a>
									<input className="cart_quantity_input" type="text"  name="quantity" value={product[key]["qty"]} autocomplete="off" size="2"/>
									<a className="cart_quantity_down" onClick={giamqty}  id={product[key]["id"]} href> - </a>
								</div>
							</td>
							<td className="cart_total">
								<p className="cart_total_price">${product[key]["price"]*product[key]["qty"]}</p>
							</td>
							<td className="cart_delete">
								<a className="cart_quantity_delete" href=""><i className="fa fa-times"></i></a>
							</td>
						</tr>
          </>
        )
      })
    }
  }
  function renderTotal(){
    let total = 0
    if(product.length >0){
      Object.keys(product).map((key,i)=>{
        total += product[key]["price"]* product[key]["qty"]
      })
    }
  
    return(  
    <ul>
      <li>Cart Sub Total <span id="totalCon">${total}</span></li>
      <li>Eco Tax <span className="tax">$2</span></li>
      <li>Shipping Cost <span>Free</span></li>
      <li>Total <span className="totalCha">${total +2}</span></li>
    </ul>)
  }
 
    function getqty(){
      let total = 0
    if(product.length >0){
      Object.keys(product).map((key,i)=>{
        total += product[key]["qty"]
      })
    }
  
   value.getqty(total)
    }
   
  
console.log(value)
    return(
      
       <div>
       {getqty()}
        <section id="cart_items">
          <div className="container">
            <div className="breadcrumbs">
              <ol className="breadcrumb">
                <li><a href="#">Home</a></li>
                <li className="active">Shopping Cart</li>
              </ol>
            </div>
            <div className="table-responsive cart_info">
              <table className="table table-condensed">
                <thead>
                  <tr className="cart_menu">
                    <td className="image">Item</td>
                    <td className="description" />
                    <td className="price">Price</td>
                    <td className="quantity">Quantity</td>
                    <td className="total">Total</td>
                    <td />
                  </tr>
                </thead>
                <tbody>
                  {renderProduct()}

                </tbody>
              </table>
            </div>
          </div>
        </section> {/*/#cart_items*/}
        <section id="do_action">
          <div className="container">
            <div className="heading">
              <h3>What would you like to do next?</h3>
              <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
            </div>
            <div className="row">
              <div className="col-sm-6">
                <div className="chose_area">
                  <ul className="user_option">
                    <li>
                      <input type="checkbox" />
                      <label>Use Coupon Code</label>
                    </li>
                    <li>
                      <input type="checkbox" />
                      <label>Use Gift Voucher</label>
                    </li>
                    <li>
                      <input type="checkbox" />
                      <label>Estimate Shipping &amp; Taxes</label>
                    </li>
                  </ul>
                  <ul className="user_info">
                    <li className="single_field">
                      <label>Country:</label>
                      <select>
                        <option>United States</option>
                        <option>Bangladesh</option>
                        <option>UK</option>
                        <option>India</option>
                        <option>Pakistan</option>
                        <option>Ucrane</option>
                        <option>Canada</option>
                        <option>Dubai</option>
                      </select>
                    </li>
                    <li className="single_field">
                      <label>Region / State:</label>
                      <select>
                        <option>Select</option>
                        <option>Dhaka</option>
                        <option>London</option>
                        <option>Dillih</option>
                        <option>Lahore</option>
                        <option>Alaska</option>
                        <option>Canada</option>
                        <option>Dubai</option>
                      </select>
                    </li>
                    <li className="single_field zip-field">
                      <label>Zip Code:</label>
                      <input type="text" />
                    </li>
                  </ul>
                  <a className="btn btn-default update" href>Get Quotes</a>
                  <a className="btn btn-default check_out" href>Continue</a>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="total_area">
                 {renderTotal()}
                  <a className="btn btn-default update" href>Update</a>
                  <a className="btn btn-default check_out" href>Check Out</a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      )
}
export default Cart