import axios from "axios";
import { useState } from "react";

function Register(){
    const [getInput, setInput] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: "",
        level: 0
    })
    const [ error, setError] = useState({})
    const [ getFile, setFile] = useState("")
    const [ getAvatar, setAvatar] = useState("")

    function handleInput(e){
        const key = e.target.name;
        const value = e.target.value;
        setInput(state => ({...state, [key]:value}))
    }

    function hanldeFile(e){
        const file = e.target.files
       
        // gui file sang api//
        let  reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result)
            setFile(file[0])
        }
        reader.readAsDataURL(file[0])
    }
    function handleSubmit(e){
        e.preventDefault();
        let errorSubmit= {};
        let flag = true;

        if(getInput.name === ""){
            errorSubmit.name = "vui long nhap ten"
            flag = false
        }
        if(getInput.email === ""){
            errorSubmit.email = "vui long nhap ten"
            flag = false
        }
        if(getInput.password === ""){
            errorSubmit.password = "vui long nhap ten"
            flag = false
        }
        if(getInput.phone === ""){
            errorSubmit.phone = "vui long nhap ten"
            flag = false
        }
        if(getInput.address === ""){
            errorSubmit.address = "vui long nhap ten"
            flag = false
        }

        if(getFile === ""){
            errorSubmit.file = "vui  long tai flie len"
            flag = false
        }
        
        if(getFile['size']> 1024*1024){
            errorSubmit.size= "vui long tai anh co dung luong be hon"
            flag = false
        }
        let  nameFile = getFile['name']
        let duoi = nameFile.split('.')
        let check = ["png", "jpg", "jpeg", "PNG", "JPg"]
        
        for( let i = 0; check.length> i ; i++){
        if(duoi[1] !== check[i] ){
            
            flag= true
            break
            
        } else {
            errorSubmit.file = "vui long tai file len gdsh"
            flag = false
            console.log(check[0])
        }}
        if(!flag){
            setError(errorSubmit)
        }
        if(flag){
            setError("")
         
            let register = {
              name: getInput.name,
              email : getInput.email,
              password : getInput.password,
              phone : getInput.phone,
              address : getInput.address,
              avatar : getAvatar,
              level: getInput.level
            }

             axios.post("http://localhost/laravel8/laravel8/public/api/register", register)
             .then(response=>{
              
               if(response.data.errors){
                setError(response.data.errors)
               } else{
                console.log(response)
               }
           
             })
             .catch(function (error) {
                console.log(error)
             })
        }
    }
    function renderError(){
        if(Object.keys(error).length> 0){
            return Object.keys(error).map((key, index)=>{
                return <li>{error[key]}</li>
            })
        }
    }



    return(
    //   form dang ky //
        <div className="signup-form">
          <h2>New User Signup!</h2>
          <form onSubmit={handleSubmit} enctype="multipart/form-data" >
            <input  onChange={handleInput} type="text" placeholder="Name" name="name" /> 
            <input onChange={handleInput} type="email" placeholder="Email Address" name="email" /> 
            <input onChange={handleInput} type="password" placeholder="password" name="password" /> 
            <input onChange={handleInput} type="number" placeholder="phone" name="phone" /> 
            <input onChange={handleInput} type="text" placeholder="Address" name="address" /> 
            <input id="test_input" type="file" name="file"  onChange={hanldeFile} /> 
            <button type="submit" className="btn btn-default">signup</button>
            {renderError()}
          </form>
        </div>
     )
}
export default Register