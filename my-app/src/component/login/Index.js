import Register from "./Register1";
import Login from "./Login"
function Index(){
    return(
        <section id="form">{/*form*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-4 col-sm-offset-1">
              <Login></Login>
            </div>
            <div className="col-sm-1">
              <h2 className="or">OR</h2>
            </div>
            <div className="col-sm-4">
              <Register></Register>
            </div>
          </div>
        </div>
      </section>
    )
}
export default Index