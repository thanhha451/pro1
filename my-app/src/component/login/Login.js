import { useState } from "react"
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Login(){
    let navigate = useNavigate()
    const [data, getData] = useState({
        email: "",
        password:"",
        level:0
    })
    const [error, setError] = useState({})
    function handleData(e){
        const key = e.target.name;
        const value = e.target.value;
        getData(state => ({...state, [key]:value}))
    }
    function handleSubmit(e){
        e.preventDefault();
        
        let errorSubmit = {}
        let flag = true
        if(data.email === ""){
            errorSubmit.email = "vui long nhap email"
            flag =false
        }
        if(data.password === ""){
            errorSubmit.password = "vui long nhap pass"
            flag =false
        }
        if(!flag){
            setError(errorSubmit)
        }
        if(flag){
            setError("")
            
            

            const dataBase = {
                email: data.email,
                password: data.password,
                level: 0
            }
            console.log(dataBase)
            axios.post("http://localhost/laravel8/laravel8/public/api/login", dataBase)
             .then(response=>{
                
               if(response.data.response){
                    setError(response.data.errors)
               } else{
                    navigate("/")
                    console.log(response)
                
                    localStorage.setItem(true, JSON.stringify(response.data) )
               }
              
           
             })
             .catch(function (error) {
                console.log(error)
             }) 
        }
    }
    function renderError(){
        if(Object.keys(error).length> 0){
            return Object.keys(error).map((key, index)=>{
                return <li>{error[key]}</li>
            })
        }
    }
    
    return (
        <div className="login-form">{/*login form*/}
                <h2>Login to your account</h2>
                <form onSubmit={handleSubmit}  enctype="multipart/form-data" action="#">
                  <input  type="email" placeholder="Name" name="email" onChange={handleData} />
                  <input  type="password" placeholder="password" name="password" onChange={handleData} />
                 
                  <span>
                    <input type="checkbox" className="checkbox" /> 
                    Keep me signed in
                  </span>
                  <button  type="submit" className="btn btn-default"><a href>login</a></button>
                  {renderError()}
                  
                </form>
              </div>
    )
}
export default Login