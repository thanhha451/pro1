import axios from "axios";
import { useState } from "react";

 function Account(){
  
    const userData = JSON.parse(localStorage.getItem("true"))
    const [getInput, setInput] = useState({
      name: userData.Auth.name,
      email: userData.Auth.email,
      password: "",
      phone: userData.Auth.phone,
      address: userData.Auth.address,
      level: 0
    })
    const [ error, setError] = useState({})
    const [ getFile, setFile] = useState("")
    const [ getAvatar, setAvatar] = useState("")
    
    function handleInput(e){
      const key = e.target.name;
      const value = e.target.value;
      setInput(state => ({...state, [key]:value}))
    }
    
    function hanldeFile(e){
      const file = e.target.files
     
      // gui file sang api//
      let  reader = new FileReader();
      reader.onload = (e) => {
          setAvatar(e.target.result)
          setFile(file[0])
      }
      reader.readAsDataURL(file[0])
    }
    function handleSubmit(e){
      e.preventDefault();
      let errorSubmit= {};
      let flag = true;
    
      if(getInput.name === ""){
          errorSubmit.name = "vui long nhap ten"
          flag = false
      }
      if(getInput.email === ""){
          errorSubmit.email = "vui long nhap email"
          flag = false
      }
      if(getInput.password === ""){
          errorSubmit.password = "vui long nhap pass"
          flag = false
      }
      if(getInput.phone === ""){
          errorSubmit.phone = "vui long nhap phone"
          flag = false
      }
      if(getInput.address === ""){
          errorSubmit.address = "vui long nhap dia chi"
          flag = false
      }
      if(localStorage.getItem("true") ==""){
        errorSubmit.login =" vui long dang nhap"
        flag = false
      }
    console.log(getFile)
      if(getFile === ""){
          errorSubmit.file = "vui  long tai flie len"
          flag = false
      } else{
        let  nameFile = getFile['name']
      let duoi = nameFile.split('.')
      let check = ["png", "jpg", "jpeg", "PNG", "JPg"]
      
      for( let i = 0 ; check.length< i ; i++){
        if(duoi["1"] !== check[i]){
          errorSubmit.file="vui long tai file anh"
          flag =false
        } else {
          flag = true
          break
        }
      }
      }
      
      if(getFile['size']> 1024*1024){
          errorSubmit.size= "vui long tai anh co dung luong be hon"
          flag = false
      }
      
    
      if(!flag){
          setError(errorSubmit)
      }
      if(flag){
          setError("")
        console.log(getInput)
        let updateUser = {
          name: getInput.name,
          email : getInput.email,
          password : getInput.password,
          phone : getInput.phone,
          address : getInput.address,
          avatar : getAvatar,
          level: getInput.level
        }
        let accessToken = userData.token
                   
        let config = { 
        headers: { 
        'Authorization': 'Bearer '+ accessToken,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
      };
    
         axios.post("http://localhost/laravel8/laravel8/public/api/user/update/" + userData.Auth.id, updateUser, config)
         .then(response=>{
          console.log(response)
       
         })
         .catch(function (error) {
            console.log(error)
         })  
        }
       
         
    }
    function renderError(){
      if(Object.keys(error).length> 0){
          return Object.keys(error).map((key, index)=>{
              return <li>{error[key]}</li>
          })
      }
    }
    
    

 


 

    return(
       
            <div className="col-sm-4 formUp">
              <div className="signup-form">
                  <h2>New User Signup!</h2>
                <form onSubmit={handleSubmit}  enctype="multipart/form-data" >
                  <input  onChange={handleInput} type="text" placeholder="Name" name="name" defaultValue={userData.Auth.name} /> 
                  <input readOnly type="email" placeholder="Email Address" name="email" value={userData.Auth.email} /> 
                  <input onChange={handleInput} type="password" placeholder="password" name="password" defaultValue={userData.Auth.password} /> 
                  <input onChange={handleInput} type="number" placeholder="phone" name="phone" defaultValue={userData.Auth.phone} /> 
                  <input onChange={handleInput} type="text" placeholder="Address" name="address" defaultValue={userData.Auth.address} /> 
                  <input id="test_input" type="file" name="file" onChange={hanldeFile}  /> 
                  <button type="submit" className="btn btn-default">signup</button>
                {renderError()}
                </form>
              </div>
            </div>
      
    )
    }
 export default Account