import axios from "axios"
import { useState, useEffect } from "react"
function MyProduct(){
    const [product, setProduct] = useState([])
    const userData = JSON.parse(localStorage.getItem("true")) 
    let accessToken = userData.token
            
        let config = { 
        headers: { 
        'Authorization': 'Bearer '+ accessToken,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
      }; 
    useEffect(() =>{
          
        
        axios.get('http://localhost/laravel8/laravel8/public/api/user/my-product', config)
        .then(response => {
  
          setProduct(response.data.data)
        })
        .catch((error)=>{console.log(error)})
    },[])
  
        function deleteProduct(e){
            axios.get("http://localhost/laravel8/laravel8/public/api/user/product/delete/" + e.target.id, config)
            .then((response)=>{
                setProduct(response.data.data)
            })
            .catch((error)=>{console.log(error)})
        }
        

           
  
       
        function renderProduct(){
            if(Object.keys(product).length > 0 ){
                
               return Object.keys(product).map((key, i)=>{
                    return (
                        <tr>
                            <td>{product[key]["id"]}</td>
                            <td>{product[key]["name"]}</td>
                            <td className="imgProduct"><img  src={"http://localhost/laravel8/laravel8/public/upload/product/"+ userData.Auth.id+"/" +JSON.parse(product[key]["image"])["0"] }/></td>
                            <td>{product[key]["price"]}$</td>
                            <td><a href= {"/account/editproduct/" + product[key]["id"]  }  >+</a></td>
                            <td><button onClick={deleteProduct} id={product[key]["id"]} >X</button></td>
                        </tr>
                    )
                })
            }
        }
        
        console.log(product)
        
        return(
            <div className="product">
                <table >
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                    {renderProduct()}
                </table>
                <button><a href="/account/upproduct">Add new</a></button>
            </div>
        )   
    
}
export default MyProduct