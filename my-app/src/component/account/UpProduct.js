import { useState, useEffect } from "react"
import axios from "axios"

function UpProduct(){
    const [getStatus,setStatus]= useState(1)
    const [getBrand, setBrand] = useState([])
    const [getCategory, setCategory] = useState([])
    const [getInput, setInput]= useState({
        name: "",
        price: "",
        brand: "",
        category: "",
        sale: "",
        company: "",
        detail: "",
        getFile: "",
      
    })
    const [getFile, setFile]= useState({  })
    const [ error, setError] = useState({})
    useEffect(() =>{
        axios.get('http://localhost/laravel8/laravel8/public/api/category-brand')
        .then(response => {
  
            setBrand(response.data.brand)
            setCategory(response.data.category)
        })
        .catch((error)=>{console.log(error)})
    },[])

    function renderCategory(){
        if(getCategory.length > 0){
            return Object.keys(getCategory).map((value, i) =>{
                return(
                    <option value={getCategory[value]["id"]}>{getCategory[value]["category"]}</option>
                )
            })
        }
    }
    function renderBrand(){
        if(getBrand.length > 0){
            return Object.keys(getBrand).map((value, i) =>{
                return(
                    <option value={getBrand[value]["id"]}>{getBrand[value]["brand"]}</option>
                )
            })
        }
    }
    
    function handleStatus(e){
        setStatus(e.target.value)
    }
    function renderStatus(){
        
        if( getStatus == 1){
            return("")
        } else {
            
            return(<input onChange={handleInput} type="text"  placeholder="Sale" name="sale"/>)
        } 
        
    }
   
    function handleInput(e){
        const key = e.target.name
        const value = e.target.value
        setInput(state => ({...state, [key]:value}))

    }
    function handleFile(e){
        const file = e.target.files
        setFile(file)
    }
     function handleSubmit(e){
        e.preventDefault();
        let errorSubmit= {};
       
        let flag = true;

        if(getInput.name === ""){
            errorSubmit.name = "vui long nhap ten san pham"
            flag = false
        }
        if(getInput.price === ""){
            errorSubmit.price = "vui long nhap gia ban"
            flag = false
        }
        if(getInput.brand === ""){
            errorSubmit.brand = "vui long chon thuong hieu"
            flag = false
        }
        if(getInput.category === ""){
            errorSubmit.category = "vui long chon category"
            flag = false
        }
        if(getStatus === 0 &  getInput.sale === ""){
            errorSubmit.sale = "vui long nhap gia giam"
            flag = false
        }
        if(getInput.company === ""){
            errorSubmit.company = "vui long nhap cong ty"
            flag = false
        }
        if(getInput.detail === ""){
            errorSubmit.detail = "vui long nhap noi dung"
            flag = false
        }

        if(getFile === ""){
            errorSubmit.file = "vui  long tai flie len"
            flag = false
        }
        if(getFile.length >3){
            errorSubmit.file = "vui  long upload file so luong tu 3 file tro xuong"
            flag = false
        }
        
      
        
        
        if(getFile.length > 0){
             Object.keys(getFile).map((key, i)=>{
                let  nameFile = getFile[key]["name"].split(".")[1]
                let check = ["png", "jpg", "jpeg", "PNG", "JPg"]
                for( let i = 0; check.length> i ; i++){
                    if(nameFile === check[i] ){
                        flag= true
                        if(getFile[key]["size"] > 1024*1024){
                            errorSubmit.size= "vui long tai anh co dung luong be hon"
                            flag = false
                        }
                        break
                    } else {
                        errorSubmit.file = "vui long tai file anh len"
                        flag = false
                        
                    }}
                    return (0)

            })
        }
        if(!flag){
            setError(errorSubmit)
            
        }
        if(flag){
            setError("")
               const userData = JSON.parse(localStorage.getItem("true"))

            let url = 'http://localhost/laravel8/laravel8/public/api/user/product/add'
            let accessToken = userData.token
            
              let config = { 
              headers: { 
              'Authorization': 'Bearer '+ accessToken,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json'
              } 
            };
            
            const formData = new FormData();
              formData.append("name", getInput.name)
              formData.append("price", getInput.price)
              formData.append("category",  getInput.category)
              formData.append("brand",getInput.brand)
              formData.append("detail", getInput.detail)
              formData.append("company", getInput.company)
              formData.append("status", getStatus)
              formData.append("sale", getInput.sale)
              Object.keys(getFile).map((key, i)=>{
                return formData.append("file[]", getFile[key])
              })
           console.log(formData.getAll("file[]"))

              axios.post(url, formData, config)
              .then(response=>{
                  
             console.log(response)
              }) 
              .catch(function (error) {
                console.log(error)
             })
            
        }
        
     }
     function renderError(){
        if(Object.keys(error).length> 0){
            return Object.keys(error).map((key, index)=>{
                return <li>{error[key]}</li>
            })
        }
    }
    return (
        <div className="col-sm-6 account">
            <form onSubmit={handleSubmit} enctype="multipart/form-data">
                <input onChange={handleInput} type="text"  placeholder="Name" name="name"/>
                <input onChange={handleInput} type="text"  placeholder="price" name="price"/>
                <select onChange={handleInput} name="brand">
                    <option>chose brand</option>
                    {renderBrand()}
                </select>
                <select onChange={handleInput} name="category">
                    <option>chose category</option>
                    {renderCategory()}
                </select>
                <select onChange={handleStatus} name="status" value={getStatus} >
                    <option value={1}>new</option>
                    <option value={0}>sale</option>
                </select>
                {renderStatus()}
                <input onChange={handleInput} type="text"  placeholder="Company profile" name="company"/>
                <input onChange={handleFile} type="file"  name="file" multiple/>
                <input onChange={handleInput} type="text"  placeholder="Detail" name="detail"/>
                <button type="submit">signup</button>
               
            </form>
            {renderError()}
        </div>
    )
}
export default UpProduct