
function ListComment(props){

    const listcmt = props.listcmt
      
  
  function renderRepComment(e){
      props.id_comment(e.target.id)
    
    
  }
    function renderComment(){
        if(Object.keys(listcmt).length>0){
            return listcmt.map((object, i) => {
              if(object.id_comment == 0){
                return (
                   <>
                   <li key={i} index={i} className="media">
                        <a className="pull-left" href="#">
                          <img className="media-object" src={"http://localhost/laravel8/laravel8/public/upload/user/avatar/" + object["image_user"]} alt="" />
                        </a>
                        <div className="media-body">
                          <ul className="sinlge-post-meta">
                            <li><i className="fa fa-user" />{object.name_user}</li>
                            <li><i className="fa fa-clock-o" /> {object.created_at}</li>
                            <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                          </ul>
                          <p>{object.comment}</p>
                          <a onClick={renderRepComment} className="btn btn-primary" id={object.id} href><i className="fa fa-reply" />Replay</a>
                        </div>
                      </li> 
                      {
                        listcmt.map((object2, j) => {
                          if(object.id == object2.id_comment){
                            return(
                              
                                <li key={j} className="media second-media">
                                <a className="pull-left" href="#">
                                  <img className="media-object" src={"http://localhost/laravel8/laravel8/public/upload/user/avatar/" + object2["image_user"]} alt="" />
                                </a>
                                <div className="media-body">
                                  <ul className="sinlge-post-meta">
                                    <li><i className="fa fa-user" />{object2.name_user}</li>
                                    <li><i className="fa fa-clock-o" /> {object2.created_at}</li>
                                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                  </ul>
                                  <p>{object2.comment}</p>
                                  <a className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                </div>
                              </li>
                              )
                      }})}
                    </>
               )}
                
            })
        }
    }
    

    return(
        <ul className="media-list">
        {renderComment()}
        
        
        </ul>		
  )
}
export default ListComment