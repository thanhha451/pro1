import axios from "axios";
import { useState } from "react"
import { useParams } from "react-router-dom";

function Comment(props){
    const id_comment = props.id_comment
    
    let params = useParams("");
    const [comment, setComment] = useState([])
    
    const [error, setError] =useState()
    function handleData(e){
        const commented = e.target.value
        setComment(commented)
        
       }
       function PostComment(){
         if(localStorage.getItem("true")){
           if(comment ===""){
             setError("vui long nhap comment")}else{
               setError("")
   
               const userData = JSON.parse(localStorage.getItem("true"))
   
               let url = 'http://localhost/laravel8/laravel8/public/api/blog/comment/' + params.id
               let accessToken = userData.token
               
                 let config = { 
                 headers: { 
                 'Authorization': 'Bearer '+ accessToken,
                 'Content-Type': 'application/x-www-form-urlencoded',
                 'Accept': 'application/json'
                 } 
               };
               
               const formData = new FormData();
                 formData.append("id_blog", params.id)
                 formData.append("id_user", userData.Auth.id)
                 formData.append("id_comment",  id_comment ? id_comment : 0)
                 formData.append("comment", comment)
                 formData.append("image_user", userData.Auth.avatar)
                 formData.append("name_user", userData.Auth.name)
                 formData.getAll("id_blog")
                
   
                 axios.post(url, formData, config)
                 .then(response=>{
                  
                   props.getCmt(response.data.data)
                  console.log(response)
                 })
   
               //  hien thi comment //
           
            
           
             }
         } else{
           setError("vui long login truoc khi comment")
         }
       
       }
       return(
          
            <div className="replay-box">
                
                <div className="row">
                    <div className="col-sm-12">
                    <h2>Leave a replay</h2>
                    <div className="text-area">
                        <div className="blank-arrow">
                        <label>Your Name</label>
                        </div>
                        <span>*</span>
                        <textarea id="reply" onChange={handleData} name="message" rows={7} defaultValue={""} />
                        <a onClick={PostComment} className="btn btn-primary" href>post comment</a>
                        {error}
                    </div>
                    </div>
                </div>
            </div>
            )
      
}
export default Comment