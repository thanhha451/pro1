import {useEffect, useState } from "react"
import axios from "axios";
import { Link } from "react-router-dom";
function DataBlog(){
    const [data, setData ] = useState([]);
    useEffect(()=>{
        axios.get("http://localhost/laravel8/laravel8/public/api/blog")
        .then(res =>{
            setData(
               res.data.blog
               
            )
           
        })
        .catch(function (error) {
            console.log(error)
       })
    },[])
    
    function renderData(){
        
        if(Object.keys(data).length >0 ){
            return data.data.map((object, i) => {
                return (
                    <div key={i} index={i} className="single-blog-post">
                  <h3>{object.title}</h3>
                  <div className="post-meta">
                    <ul>
                      <li><i className="fa fa-user" /> Mac Doe</li>
                      <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                      <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                    </ul>
                    <span>
                      <i className="fa fa-star" />
                      <i className="fa fa-star" />
                      <i className="fa fa-star" />
                      <i className="fa fa-star" />
                      <i className="fa fa-star-half-o" />
                    </span>
                  </div>
                  <a href>
                    <img  src={"http://localhost/laravel8/laravel8/public/upload/Blog/image/" + object['image']} alt="" />
                  </a>
                  <p>{object.description}</p>
                  
                  <Link  to={"/blog/detail/"+ object.id} className="btn btn-primary">Read More</Link>
                </div>
                )
            })
        }
    }
   
    return(
          
        <section>
            
        <div className="container">
          <div className="row">
            
            <div className="col-sm-9">
              <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {renderData()}
                
                <div className="pagination-area">
                  <ul className="pagination">
                    <li><a href className="active">1</a></li>
                    <li><a href>2</a></li>
                    <li><a href>3</a></li>
                    <li><a href><i className="fa fa-angle-double-right" /></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
} 
export default DataBlog