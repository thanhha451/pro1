import {useEffect, useState } from "react"
import axios from "axios";
import { useParams } from "react-router-dom";
import ListComment from "./ListComment";
import Comment from "./Comment";
import StarRating from "./RateStar";

function  DetailBlog(props){
    let params = useParams();
    const [getdata, setData] = useState([])
    
    const [listcmt, setlistcmt] =  useState({})
    const [id_comment, setid_comment] = useState()
    useEffect(() =>{
      axios.get('http://localhost/laravel8/laravel8/public/api/category-brand')
      .then(response => {

          console.log(response)
          
      })
      .catch((error)=>{console.log(error)})
  },[])

    useEffect(() =>{
        axios.get('http://localhost/laravel8/laravel8/public/api/blog/detail/' + params.id)
        .then(response => {

            setData(response.data.data)
            setlistcmt(response.data.data.comment)

            // setComment(response.data.data)
        })
        .catch((error)=>{console.log(error)})
    },[])
    
    function getCmt(data){
      
    
      
      setlistcmt(listcmt.concat(data))
    }
    function getid_comment(id){
      setid_comment(id)
    }
   
   
    
   
    function renderDetail(){
        if(getdata !== ""){
            return (
                <div className="single-blog-post">
                  <h3>{getdata.title}</h3>
                  <div className="post-meta">
                    <ul>
                      <li><i className="fa fa-user" /> Mac Doe</li>
                      <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                      <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                    </ul>
                    {/* <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span> */}
                  </div>
                  <a href>
                    <img src="images/blog/blog-one.jpg" alt="" />
                  </a>
                  {getdata.content}
                  <div className="pager-area">
                    <ul className="pager pull-right">
                      <li><a href="#">Pre</a></li>
                      <li><a href="#">Next</a></li>
                    </ul>
                  </div>
                </div>
            )
        }

    }
    return (
        
           
            <div className="col-sm-9">
              <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {renderDetail()}
              </div>{/*/blog-post-area*/}
              <div className="rating-area">
              <StarRating/>
                <ul className="tag">
                  <li>TAG:</li>
                  <li><a className="color" href>Pink <span>/</span></a></li>
                  <li><a className="color" href>T-Shirt <span>/</span></a></li>
                  <li><a className="color" href>Girls</a></li>
                </ul>
              </div>{/*/rating-area*/}
              <div className="socials-share">
                <a href><img src="images/blog/socials.png" alt="" /></a>
              </div>{/*/socials-share*/}
              
              <div className="response-area">
                <h2>3 RESPONSES</h2>

                <ListComment listcmt={listcmt} id_comment={getid_comment}  />
              </div>{/*/Response-area*/}
            <Comment getCmt={getCmt} id_comment={id_comment}/>
            </div>	
         
    )

}
export default DetailBlog