import {useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
 
import StarRatings from 'react-star-ratings';
   
        
        
  
    function StarRating() {
        const params = useParams("")
        const [tbrate, setTbRate]= useState([])
        const [rating, setRating] = useState(0);
        const [error, setError] = useState()
        const [id, setId] = useState()
        const url = "http://localhost/laravel8/laravel8/public/api/blog/rate/" + params.id
        
          //   lay rate tu api ve
           useEffect(()=>{
            axios.get("http://localhost/laravel8/laravel8/public/api/blog/rate/" + params.id)
            .then(res => {
             
              setTbRate(res.data.data)
              console.log(res)
            })
            
           }, [])
        function renderTbrate(){
          let rate1 = 0
          
          if(Object.keys(tbrate).length >0){
            Object.keys(tbrate).map((value,i) =>{
              const rate = tbrate[value]["rate"]
            
               return rate1 += rate
            })
            return (rate1/Object.keys(tbrate).length).toFixed(1)
          }
          
          
        }
       
        
        function changeRating(newRating) {
          setRating(newRating)

         //   post rate len api
        
          if(localStorage.getItem("true")){
     

            const userData = JSON.parse(localStorage.getItem("true"))
            const rate ={
                "user_id" : userData.Auth.id,
                "blog_id" : params.id,
                "rate" : newRating
            }
            let accessToken = userData.token
               
                 let config = { 
                 headers: { 
                 'Authorization': 'Bearer '+ accessToken,
                 'Content-Type': 'application/x-www-form-urlencoded',
                 'Accept': 'application/json'
                 } 
               };
            axios.post(url, rate, config)
            .then(response=> {
                console.log(response)
            })
            .catch(function (error) {
                console.log(error)
             }) 
           } else{
           setError("vui long login de danh gia")}
          
        }
        
          // rating = 2;
          return (
            <>
        <p className="showRate">{renderTbrate()} trên 5<span className="showTurn">(có {Object.keys(tbrate).length} lượt đã đánh giá) </span></p>
            <StarRatings
              rating={rating}
              starRatedColor="blue"
              changeRating={changeRating}
              numberOfStars={5}
              name='rating'
            />
            </>
          );
       
    }
 
  export default StarRating