import { Modal, Button } from "react-bootstrap";
import { useState,useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
function ProductDetail(){
    const params1 =useParams()
    const [product, setProduct] =useState([])
    const [image, setImage] =useState([])
    const [viewproduct, setViewproduct] = useState("")
    const [show, setShow] = useState(false);
    useEffect(()=>{
        axios.get("http://localhost/laravel8/laravel8/public/api/product/detail/" + params1.id)
        .then(response=>{
            setProduct(response.data.data)
            setImage(JSON.parse(response.data.data.image))
            
        })
        .catch((error)=>{console.log(error)})
    },[])
    console.log(product)
    function hadleSrc(e){
      setViewproduct(e.target.src)
    }
    const linkview ="http://localhost/laravel8/laravel8/public/upload/product/"+ product.id_user +"/" +image[0]
    function renderImage(){
     
      if(image.length >0){
        return Object.keys(image).map((key, i)=>{
          return (
            <>
            
            <a href><img onClick={hadleSrc}  src={"http://localhost/laravel8/laravel8/public/upload/product/"+ product.id_user +"/" +image[key]} alt="" /></a>
            </>
            )
        })
      }
    }

  return (
    <div>
      
      <div className="product-details">{/*product-details*/}
        <div className="col-sm-5">
          <div className="view-product">
            <img src={(viewproduct == "") ? linkview :viewproduct } alt="" />
            <a href rel="prettyPhoto"><h3>ZOOM</h3></a>
          </div>
          <div id="similar-product" className="carousel slide" data-ride="carousel">
            {/* Wrapper for slides */}
            <div className="carousel-inner">
              <div className="item active">
              {renderImage()}
              </div>
              
            </div>
            {/* Controls */}
            <a className="left item-control" href="#similar-product" data-slide="prev">
              <i className="fa fa-angle-left" />
            </a>
            <a className="right item-control" href="#similar-product" data-slide="next">
              <i className="fa fa-angle-right" />
            </a>
          </div>
        </div>
        <div className="col-sm-7">
          <div className="product-information">{/*/product-information*/}
            <img src="images/product-details/new.jpg" className="newarrival" alt="" />
            <h2>{product.name}</h2>
            <p>Web ID: {product.id}</p>
            <img src="images/product-details/rating.png" alt="" />
            <span>
              <span>US ${product.price}</span>
              <label>Quantity:</label>
              <input type="text" defaultValue={3} />
              <button type="button" className="btn btn-fefault cart">
                <i className="fa fa-shopping-cart" />
                Add to cart
              </button>
            </span>
            <p><b>Availability:</b> In Stock</p>
            <p><b>Condition:</b> New</p>
            <p><b>Brand:</b> E-SHOPPER</p>
            <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
          </div>{/*/product-information*/}
        </div>
      </div>{/*/product-details*/}
    </div>
  );

}
export default ProductDetail