import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Cart from './Cart';
import Home from './component/layout/Home';
import {
  BrowserRouter as Router,
  Routes, 
  Route,
} from "react-router-dom"
import reportWebVitals from './reportWebVitals';
import DataBlog from './component/detailblog/Index';
import DetailBlog from './component/detailblog/Detail';
import Index from './component/login/Index';
import Account from './component/account/Account';
import UpProduct from './component/account/UpProduct';
import MyProduct from './component/account/MyProduct';
import EditProduct from './component/account/EditProduct';
import ProductDetail from './component/cart/Product';
import { UserContext } from './UserContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
   
      <App>
        <Routes>
          <Route index path='/' element={<Home/>}/>
          <Route  path='/cart' element={<Cart/>}/>
          <Route path='/blog/detail/:id' element={<DetailBlog/>}/>
          <Route  path='/blog' element={<DataBlog/>}/>
          <Route path='/login' element={<Index/>}/>
          <Route path='/account/update' element={<Account/>}/>
          <Route path='/account/myproduct' element={<MyProduct/>}/>
          <Route path='/account/upproduct' element={<UpProduct/>}/>
          <Route path='/account/editproduct/:id' element={<EditProduct/>}/>
          <Route path='/productdetail/:id' element={<ProductDetail/>}/>
        </Routes>
      </App>
      
    </Router>
    
  
 
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
