import { useState } from "react";
 function Form(){
    const [inputs, setInput] = useState({
        email : "",
        password : ""
    })
    const [error , setError] = useState({})

    function HandleInput(e){
        const nameInput = e.target.name
        const valueInput = e.target.value

        setInput(state => ({...state, [nameInput]:valueInput}))
    }

    function HandleSubmit(e){
        e.preventDefault();
        let errorSubmit = {}
        let flag = true;

        if(inputs.email === ""){
            errorSubmit.email =" vui long nhap mail"
            flag = false;
        }
        if(inputs.password === ""){
            errorSubmit.password =" vui long nhap pass"
            flag = false;
        }
        if(!flag){
            setError( errorSubmit)
        }
        
        
    }
    function RenderError(){
        if(Object.keys(error).length > 0){
            return Object.keys(error).map((key,index)=>{
                return (
                    <li key={index}>{error[key]}</li>
                )
            })
        }
    }
    return (
        <div>
             <p>{RenderError()}</p>
            <form onSubmit={HandleSubmit} >
                <input type="text" placeholder="Email" name="email" onChange={HandleInput} />
                <input type="password" name="password" onChange={HandleInput} />
                <button type="submit" >Login</button>

            </form>
        </div>
    )
}
 export default Form
